package com.cm55.moduleChk;

import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.jar.*;
import java.util.zip.*;

import com.cm55.moduleChk.Analy.*;

/**
 * 一つのjarファイルについて、その「モジュール性」を取得する
 * 
 * @author Owner
 */
public class ModuleChecker {

  public static final String AUTOMATIC_MODULE_NAME = "Automatic-Module-Name";
  public static final String MODULE_INFO_CLASS = "module-info.class";
  public static final Path TEMP_DIR = Paths.get(System.getProperty("java.io.tmpdir"));
  public static final ModuleInfoReader moduleInfoReader = new ModuleInfoReader();
  
  public Result check(Path path, String filename) throws IOException {
    // レギュラーファイルであること
    if (!Files.isRegularFile(path))
      throw new IOException("Not a regular file:" + path);

    // .jarで終わってること
    if (!path.toString().endsWith(".jar"))
      throw new IOException("Not ends with .jar:" + path);

    // jarファイルをオープン
    Result result = new Result(filename);
    try (JarFile file = new JarFile(path.toFile())) {

      // Automatic-Module-Name属性を取得
      result.automaticModuleName = readAutomaticModuleName(file);
      
      // module-info.classの内容を取得
      result.moduleInfoClass = readModuleInfoClass(file);
      
      return result;
    }
  }
  
  Optional<String>readAutomaticModuleName(JarFile file) throws IOException {
    Manifest man = file.getManifest();
    
    if (man == null) return Optional.empty();
    
    // Automatic-Module-Name属性取得
    Attributes attrs = man.getMainAttributes();
    return Optional.ofNullable(attrs.getValue(AUTOMATIC_MODULE_NAME));    
  }
  
  /** 
   * {@link JarFile}中の"module-info.class"の内容を読み込む
   * @param in
   * @return
   */
  Optional<String> readModuleInfoClass(JarFile file) throws IOException {
    ZipEntry entry = file.getEntry(MODULE_INFO_CLASS);
    if (entry == null) return Optional.empty();
        
    if (!moduleInfoReader.isEnabled()) {
      return Optional.of("module-info.class exists");
    }
    
    try (InputStream in = file.getInputStream(entry)) {
      Path tempFile = Files.createTempFile(TEMP_DIR, "moduleChk-", ".class");
      Files.delete(tempFile);
      try {
        Files.copy(in, tempFile);    
        try {
          String read = moduleInfoReader.read(tempFile);

          return Optional.of(read);
        } catch (IOException ex) {
          ex.printStackTrace();
          return Optional.of("ERROR:" + ex.getMessage());
        }
      } finally {
        //Files.delete(tempFile);
      }
    }    
  }

}

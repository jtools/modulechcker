package com.cm55.moduleChk;

import java.util.*;

public class Analy {


  /** 取得結果 */
  public static class Result {
    public final String filename;
    public Optional<String> automaticModuleName = Optional.empty();
    public Optional<String> moduleInfoClass = Optional.empty();
    public Result(String filename) {
      this.filename = filename;
    }
    
    @Override 
    public String toString() {
      return "==== " + filename + 
        automaticModuleName
          .map(n-> "\n" + "Automatic-Module-Name: " + n.toString()).orElse("") +
        moduleInfoClass
          .map(n-> "\n " + "module-info.class:" + n).orElse("") +
          "\n";
    }
  }

}

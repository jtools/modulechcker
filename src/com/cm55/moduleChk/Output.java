package com.cm55.moduleChk;

public interface Output {

  void clear();
  void add(String value);

}

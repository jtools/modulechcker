package com.cm55.moduleChk;

import java.io.*;
import java.util.concurrent.*;

/**
 * コマンドを実行し、その結果を取得する
 * @author ysugimura
 */
public class ProcessExec {

  public final int ret;
  public final String stdout;
  public final String stderr;

  /**
   * コマンドを実行し、終了を待つ。
   * 結果はret, stdout, stderrに設定される。
   * @param cmd 実行するコマンド
   * @throws IOException
   */
  public ProcessExec(String...cmd) throws IOException {
    
    StringBuilder stdoutBuf = new StringBuilder();
    StringBuilder stderrBuf = new StringBuilder();
    
    Process process = new ProcessBuilder(cmd).start();
    readInputStream(process.getInputStream(), stdoutBuf);
    readInputStream(process.getErrorStream(), stderrBuf);        
    try {
      process.waitFor();
    } catch (InterruptedException ex) {}
    stdout = stdoutBuf.toString();
    stderr = stderrBuf.toString();
    ret = process.exitValue();
  }

  /** 指定された入力ストリームを読み込み、StringBuilderに書き込む */
  void readInputStream(InputStream in, StringBuilder buf) throws IOException {
    BufferedReader r = new BufferedReader(new InputStreamReader(in));
    ExecutorService service = Executors.newSingleThreadExecutor();
    service.submit(()-> {
      while (true) {
        String line = r.readLine();
        if (line == null) break;
        buf.append(line + "\n");
      }
      r.close();
      return null;
    });
    service.shutdown();   
  }   
}

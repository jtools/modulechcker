package com.cm55.moduleChk;

import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.io.*;
import java.nio.file.*;
import java.util.*;
import java.util.List;
import java.util.stream.*;

import javax.swing.*;

import com.cm55.moduleChk.Analy.*;
import com.cm55.moduleChk.ModuleChecker.*;
import com.cm55.sg.*;

public class Main {

  ModuleChecker checker = new ModuleChecker();
  
  Main() {    
    SgFrame frame =  new SgFrame("ModuleChecker " + Version.version)
        .setExitOnClose()
        .setBounds(200, 50, 400, 600)
        .setLayout(new BorderLayout());
    
    SgLabel label;
    SgTextArea textArea = new SgTextArea().setEditable(false);
    frame.setLayout(new SgBorderLayout.V(        
      label = new SgLabel("Drop a jar file or folder which contains multiple jar files"),
      textArea,
      null
    ));
    label.setPreferredSize(new Dimension(200, 50));
    
    Output output = new Output() {

      @Override
      public void clear() {
        textArea.setText("");
      }

      @Override
      public void add(String value) {
        textArea.setText(textArea.getText() + value);
      }      
    };
    
    setupDrop(label.w(), output);
    
    frame.setVisible(true);
  }
  
  private void setupDrop(JComponent component, Output output) {
    DropTargetListener dtl = new DropTargetAdapter() {
      @Override public void dragOver(DropTargetDragEvent dtde) {
        if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
          dtde.acceptDrag(DnDConstants.ACTION_COPY);
          return;
        }
        dtde.rejectDrag();
      }
      @Override public void drop(DropTargetDropEvent dtde) {
        try {
          if (dtde.isDataFlavorSupported(DataFlavor.javaFileListFlavor)) {
            dtde.acceptDrop(DnDConstants.ACTION_COPY);
            Transferable transferable = dtde.getTransferable();
            
            @SuppressWarnings("unchecked")
            List<Object> list = 
              (List<Object>)transferable.getTransferData(DataFlavor.javaFileListFlavor);
            
            list.stream()
              .map(o->(File)o)            
              .filter(file->file.exists())            
              .map(file->file.toPath())
              .forEach(path->processPath(path, output));
            
            dtde.dropComplete(true);
            return;
          }
        } catch (UnsupportedFlavorException | IOException ex) {
          ex.printStackTrace();
        }
        dtde.rejectDrop();
      }
    };
    new DropTarget(component, DnDConstants.ACTION_COPY, dtl, true);
  }
  
  void processPath(Path topPath, Output output) {
    output.clear();
    if (Files.isDirectory(topPath)) {
      int topPathLength = topPath.toString().length() + 1;
      try {
        Files.walk(topPath).sorted().filter(p -> p.toString().endsWith(".jar")).forEach(p -> {
          try {
            Result r = checker.check(p, p.toString().substring(topPathLength));
            output.add(r.toString() );
          } catch (IOException ex) {

          }
        });
      } catch (IOException ex) {
        
      }
    } else {
      try {
        Result r = checker.check(topPath, topPath.getFileName().toString());
        output.add(r.toString());
      } catch (IOException ex) {

      }
    }
  }
  
  public static void main(String[]args) {
    new Main();
  }
}

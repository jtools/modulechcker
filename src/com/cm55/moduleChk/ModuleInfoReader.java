package com.cm55.moduleChk;

import java.io.*;
import java.nio.file.*;

/**
 * module-info.classの内容をjavapで取得する
 * @author ysugimura
 */
public class ModuleInfoReader {

  private int majorVersion;
  private boolean enabled;
  
  /**
   * 現在のjavapが"module-info.class"の解析をサポートしているかを識別する
   * Java9以前にはこの機能は無い。
   */
  public ModuleInfoReader() {
    try {
      ProcessExec result = new ProcessExec("javap", "-version");
      // Java9以降は"12.0.1"などの文字列、以前は"1.8.0_212"などの文字列
      majorVersion = Integer.parseInt(result.stdout.split("\\.")[0]);
      enabled = majorVersion > 1;
    } catch (Exception ex) {
      enabled = false;
    }
  }

  public int getMajorVersion() {
    return majorVersion;
  }
  
  public boolean isEnabled() {
    return enabled;
  }

  /**
   * 指定されたclassファイルの内容をjavapを呼び出してテキストとして取得する。
   * @param classFile javaクラスファイル
   * @return
   * @throws IOException ファイルが読み込めない、あるいは解釈不能
   */
  public String read(Path classFile) throws IOException {
    if (!enabled) throw new RuntimeException(
      ModuleInfoReader.class.getSimpleName() + " is disabled");
    
    ProcessExec result = new ProcessExec(
        "javap",
        classFile.toString()
    );
    if (result.ret != 0) {
      throw new IOException("Could not process module-info.class, perhaps Javap version is under Java9");
    }
    return result.stdout;
  }
}
